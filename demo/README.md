# QSortableTree demo

Example usage of QSortableTree

## Demo

https://mechanicalgux.gitlab.io/quasar-sortable-tree/


## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```
