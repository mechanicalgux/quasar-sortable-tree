/**
 * Quasar App Extension index/runner script
 * (runs on each dev/build)
 *
 * Docs: https://quasar.dev/app-extensions/development-guide/index-api
 * API: https://github.com/quasarframework/quasar/blob/master/app/lib/app-extension/IndexAPI.js
 */

function extendWithComponent (conf) {
    conf.boot.push('~quasar-app-extension-qsortabletree/src/boot/register.js');
    conf.build.transpileDependencies.push(/quasar-app-extension-qsortabletree[\\/]src/);
}

module.exports = function (api) {
    api.registerDescribeApi('', './components/QSortableTree.json')

    api.compatibleWith('quasar', '^1.7.0');
    api.compatibleWith('@quasar/app', '^1.4.3 || ^2.0.0');

    api.extendQuasarConf(extendWithComponent);
}
