{
    "props": {
        "nodes": {
            "type": "Array",
            "category": "content",
            "desc": "The array of nodes that designates the tree structure",
            "required": true,
            "examples": ["[ {...}, {...} ]" ]
        },
        "node-key": {
            "type": "String",
            "category": "content",
            "desc": "The property name of each node object that holds a unique node id",
            "required": true,
            "examples": ["key", "id"]
        },
        "label-key": {
            "type": "String",
            "category": "content",
            "desc": "The property name of each node object that holds the label of the node",
            "default": "label",
            "examples": ["name", "description"]
        },
        "children-key": {
            "type": "String",
            "category": "content",
            "desc": "The property name of each node object that holds the children of the node",
            "default": "children",
            "examples": ["children", "subItems"]
        },
        "leaf-key": {
            "type": "String",
            "category": "content",
            "desc": "The property name of each node object that determines whether or not a node is a leaf. Leave nodes can't have children. By default all nodes can have children.",
            "default": "leaf",
            "examples": ["leaf", "isLeaf"]
        },
        "no-connectors": {
            "type": "Boolean",
            "category": "style",
            "default": false,
            "desc": "Do not display the connector lines between nodes"
        },
        "drag-area-height": {
            "type": "Number",
            "category": "style",
            "default": 1,
            "desc": "Height of the drag area of each nodes"
        },
        "default-expand-all": {
            "type": "Boolean",
            "category": "behavior",
            "default": false,
            "desc": "Do not display the connector lines between nodes"
        },
        "selected": {
            "type": "String",
            "category": "state",
            "desc": "Key of node currently selected"
        }
    },
    "scopedSlots": {
        "default-header": {
            "desc": "Slot to use for defining the header of a node"
        },
        "default-body": {
            "desc": "Slot to use for defining the body of a node"
        }
    },
    "events": {
        "change": {
            "desc": "Emitted when any of the nodes changed, regardless how deep they are",
            "params": {
                "nodes": {
                    "type": "Object",
                    "desc": "The new nodes",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "select": {
            "desc": "Emitted when a node is selected with left-click",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The selected node",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "dblclick": {
            "desc": "Emitted when a node is double-clicked",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The double-clicked node",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "rightclick": {
            "desc": "Emitted when a node is right-clicked",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The right-clicked node",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "mouseenter": {
            "desc": "Emitted when the mouse enters a node",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The entered node",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "mouseleave": {
            "desc": "Emitted when the mouse leaves a node",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The left node",
                    "__exemption": [ "examples" ]
                }
            }
        },
        "mouseover": {
            "desc": "Emitted when the mouse moves over a node",
            "params": {
                "node": {
                    "type": "Object",
                    "desc": "The hovered node",
                    "__exemption": [ "examples" ]
                }
            }
        }
    }
}
