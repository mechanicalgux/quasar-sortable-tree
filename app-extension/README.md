QSortableTree
===

QSortableTree simulates QTree look and feel with drag&drop ability.

# Install
```bash
quasar ext add qsortabletree
```
Quasar CLI will retrieve it from NPM and install the extension.

# Uninstall
```bash
quasar ext remove qsortabletree
```

# Usage

```vue
<q-sortable-tree :nodes="nodes"
                 node-key="id"
                 label-key="name"
                 no-connectors
                 :default-expand-all="true"/>
```
