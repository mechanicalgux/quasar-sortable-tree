# QSortableTree

## Live demo

https://mechanicalgux.gitlab.io/quasar-sortable-tree/


## Quasar app extension

See [app-extension](app-extension) folder


## Demo source code

See [demo](demo) folder
